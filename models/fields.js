const mongoose = require('mongoose')

let static_fields = mongoose.Schema({
    name: {
        type: String,
        required: true,
    }
})

var genres = mongoose.model('Genres', static_fields)
var gig_type = mongoose.model('Gigs', static_fields)
var state = mongoose.model('State', static_fields)
var booker_type = mongoose.model('BookerType', static_fields)

module.exports = {genres,
    gig_type,
    state,
    booker_type}