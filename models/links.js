const mongoose = require('mongoose')

let links_schema = mongoose.Schema({
    type: {
        type: String,
        required: true,
    },
    url: {
        type: String,
        required: true
    }
})

module.exports = {links_schema}