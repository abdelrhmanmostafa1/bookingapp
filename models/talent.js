const mongoose = require('mongoose')
const {links_schema} = require('./links')

let talent_schema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    bio: {
        type: String,
        required: true
    },
    location: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true
    },
    price_from: {
        type: String,
        required: true
    },
    price_to: {
        type: String,
        required: true
    },
    size: {
        type: Number,
        required: true
    },
    featured_songs: {
        type: [links_schema],
        required: true
    },
    social_links: {
        type: [links_schema],
        required: true
    },
    genres: {
        type: [mongoose.SchemaTypes.ObjectId],
        required: true
    },
    prefered_gig_types: {
        type: [mongoose.SchemaTypes.ObjectId],
        required: true
    },
    willing_to_travel: {
        type: [mongoose.SchemaTypes.ObjectId],
        required: true
    },
    photo: {
        type: {
            url:{
                type: String,
                required:true
            },
            thumb:{
                type: String,
                required:true
            },
            header: {
                type: String,
                required: true
            },
            required: true
        }
    }
})

module.exports = {talent_schema}