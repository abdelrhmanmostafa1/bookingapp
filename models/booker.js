const mongoose = require('mongoose')

let booker_schema = mongoose.Schema({
    fname:{
        type: String,
        required: true
    },
    lname:{
        type: String,
        required: true
    },
    photo:{
        type: {
            url:{
                type: String,
                required:true
            },
            thumb:{
                type: String,
                required:true
            }
        },
        required: true
    },
    favourite_genres: {
        type: [mongoose.SchemaTypes.ObjectId]
    },
    booker_type: {
        type: [mongoose.SchemaTypes.ObjectId]
    },
    favourite_bands: {
        type: [mongoose.SchemaTypes.ObjectId]
    },
    event_list: {
        type: [mongoose.SchemaTypes.ObjectId]
    }
})

module.exports = {booker_schema}